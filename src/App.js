import Buttons from './components/button';
import Show from './components/counter';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Buttons/>
        <Show/>
      </header>
    </div>
  );
}

export default App;
