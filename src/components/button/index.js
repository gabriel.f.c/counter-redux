import { Button, ButtonGroup } from '@material-ui/core'
import { addNum, subNum } from '../../store/modules/counter/actions'
import { useDispatch } from 'react-redux'

function Buttons() {
    const dispatch = useDispatch()

    const handleAdd = () => {
        dispatch(addNum(1))
    }

    const handleSub = () => {
        dispatch(subNum(1))
    }

    return (
        <ButtonGroup color="primary" aria-label="outlined primary button group">
            <Button onClick={() => handleAdd()}>Add</Button>
            <Button onClick={() => handleSub()}>Sub</Button>
        </ButtonGroup>
    )
}


export default Buttons