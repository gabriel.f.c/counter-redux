import { useSelector } from "react-redux";

function Show() {

    const showNum = useSelector(state => state.counter)

    return (
        <div>{showNum}</div>
    )
}

export default Show